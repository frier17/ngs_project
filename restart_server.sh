#!/bin/bash
source /home/ubuntu/ngs/ngs_project
cd /home/ubuntu/ngs/ngs_project
PID=$(ps aux | grep 'uvicorn ngs_project.asgi:fastapp' | grep -v grep | awk {'print $2'} | xargs)
if [ "$PID" != "" ]
then
kill -9 $PID
sleep 2
echo "" > nohup.out
echo "Restarting FastAPI server"
else
echo "No such process. Starting new FastAPI server"
fi
nohup uvicorn ngs_project.asgi:fastapp 

from uuid import UUID

from typing import TypeVar, Type, List, Mapping, Union, Any
from django.db import models
from django_cassandra_engine.models import DjangoCassandraModel
from fastapi import HTTPException

from pydantic import BaseModel as PydanticModel

ModelT = TypeVar("ModelT", bound=DjangoCassandraModel)


def _get_instance(model_class: Type[ModelT], identifier: Union[int, str, UUID]) -> ModelT:
    instance = None
    if isinstance(identifier, UUID):
        instance = model_class.objects.filter(uuid=identifier).first()
    elif isinstance(identifier, str):
        instance = model_class.objects.filter(
            models.Q(slug=identifier) |
            models.Q(token=identifier))
    elif isinstance(identifier, int):
        instance = model_class.objects.filter(
            models.Q(pk=identifier) |
            models.Q(id=identifier))
    return instance


def _get_instance_queryset(model_class: Type[ModelT], identifier: Union[int, str, UUID]) -> Any:
    queryset = None
    if isinstance(identifier, UUID):
        queryset = model_class.objects.filter(uuid=identifier)
    elif isinstance(identifier, str):
        queryset = model_class.objects.filter(
            models.Q(slug=identifier) |
            models.Q(token=identifier))
    elif isinstance(identifier, int):
        queryset = model_class.objects.filter(
            models.Q(pk=identifier) |
            models.Q(id=identifier))
    return queryset.first()


def model_retrieve(model_class: Type[ModelT], identifier: Union[int, str, UUID]) -> ModelT:
    instance = _get_instance(model_class, identifier)

    if not instance:
        raise HTTPException(status_code=404, detail="Object not found.")
    return instance


def model_list(model_class: Type[ModelT]) -> List[ModelT]:
    return model_class.objects.all()


def model_create(model_class: Type[ModelT], data: Union[PydanticModel, Mapping]) -> Any:
    if isinstance(data, Mapping):
        if hasattr(model_class, 'from_dict'):
            return model_class.from_dict(**data)
        else:
            try:
                model_class.objects.create(**data)
            except Exception:
                raise HTTPException(status_code=500, detail='Unable to create instance from data')
    elif isinstance(data, PydanticModel) and hasattr(model_class, 'from_schema'):
        record = model_class.from_schema(data)
        return record


def model_update(model_class: Type[ModelT], identifier: Union[int, str, UUID],
                 data: Union[PydanticModel, Mapping]) -> ModelT:
    instance = _get_instance_queryset(model_class, identifier)
    if not instance:
        raise HTTPException(status_code=404, detail="Object not found.")
    if isinstance(data, PydanticModel):
        model_fields = [str(x) for x in model_class._meta.get_fields()]
        model_data = {x: y for x, y in data.dict().items() if x in model_fields}
        instance.update(**model_data)
    elif isinstance(data, Mapping):
        data = dict(**data)
        model_fields = [str(x) for x in model_class._meta.get_fields()]
        model_data = {x: y for x, y in data.items() if x in model_fields}
        instance.update(**model_data)
    return instance


def model_destroy(model_class: Type[ModelT], identifier: Union[str, int, UUID]) -> Any:
    instance = _get_instance(model_class, identifier)
    if not instance:
        raise HTTPException(status_code=404, detail="Object not found.")
    return instance.delete()


# Define utility functions to parse between schema and model
def parse_to_schema(schema: Any, data: Union[Mapping, ModelT]) -> PydanticModel:
    if isinstance(data, Mapping):
        return schema(**data)
    else:
        data = vars(data)
        return schema(**data)


def parse_to_model(model_class: ModelT, data: Union[Mapping, PydanticModel]):
    if isinstance(data, Mapping) and hasattr(model_class, 'from_dict'):
        return model_class.from_dict(data)
    elif isinstance(data, PydanticModel) and hasattr(model_class, 'from_schema'):
        return model_class.from_schema(data)


from typing import List, Any
from fastapi import APIRouter, Depends
from fastapi.responses import HTMLResponse

from variant.models import adapters
from variant.models import schemas

variant_router = APIRouter()
sample_variant_router = APIRouter()
coverage_router = APIRouter()

home_router = APIRouter()


## Variant
@variant_router.get("/", description="List all entries of Variant from DNA Sequence records")
def list_variant(
        result: List[schemas.Variant] = Depends(adapters.list_variants)) -> List[schemas.Variant]:
    return result


@variant_router.get("/{identifier}", description="Retrieve a given record of Variant data given a unique identifier")
def retrieve_variant(model: schemas.Variant = Depends(adapters.retrieve_variant)) -> schemas.Variant:
    return model


@variant_router.post("/filter", description="Filter a range of variant")
def retrieve_filter_variant(queryset: List[schemas.Variant] = Depends(adapters.retrieve_filter_variant)) -> List[
    schemas.Variant]:
    return queryset


@variant_router.post("/", description="Create a new record for Variant using provided data")
def create_variant(model: schemas.Variant = Depends(adapters.create_variant)) -> schemas.Variant:
    return model


@variant_router.put("/update/{identifier}",
                    description="Update an existing record of Variant data given a unique identifier")
def update_variant(model: schemas.Variant = Depends(adapters.update_variant)) -> schemas.Variant:
    return model


@variant_router.post("/update/multiple", description="Update multiple records")
def multiple_update_variant(records: List[schemas.Variant] = Depends(adapters.multiple_update_variant)) -> \
        List[schemas.Variant]:
    return records


@variant_router.delete("/delete/{identifier}", description="Delete a record of Variant data given a unique identifier")
def destroy_variant(deleted: Any = Depends(adapters.destroy_variant)) -> Any:
    return deleted


# Sample Variant
@sample_variant_router.get("/", description="List all entries of SampleVariant")
def list_sample_variant(
        result: List[schemas.VariantSample] = Depends(adapters.list_sample_variant)) -> List[schemas.VariantSample]:
    return result


@sample_variant_router.get("/{identifier}",
                           description="Retrieve a given record of SampleVariant given a unique identifier")
def retrieve_sample_variant(
        model: schemas.VariantSample = Depends(adapters.retrieve_sample_variant)) -> schemas.VariantSample:
    return model


@sample_variant_router.post("/search", description="Retrieve a given record of SampleVariant given a search query")
def search_sample_variant(
        model: schemas.VariantSample = Depends(adapters.search_sample_variant)) -> schemas.VariantSample:
    return model


@sample_variant_router.post("/", description="Create a new record for SampleVariant using provided data")
def create_sample_variant(
        model: schemas.VariantSample = Depends(adapters.create_sample_variant)) -> schemas.VariantSample:
    return model


@sample_variant_router.put("/update/{identifier}",
                           description="Update an existing record of SampleVariant given a unique identifier")
def update_sample_variant(
        model: schemas.VariantSample = Depends(adapters.update_sample_variant)) -> schemas.VariantSample:
    return model


@sample_variant_router.delete("/delete/{identifier}",
                              description="Delete a record of SampleVariant given a unique identifier")
def destroy_sample_variant(deleted: Any = Depends(adapters.destroy_sample_variant)) -> Any:
    return deleted


# Coverage
@coverage_router.get("/", description="List all entries of Coverage")
def list_coverage(
        model: List[schemas.Coverage] = Depends(adapters.list_coverage)) -> List[schemas.Coverage]:
    return model


@coverage_router.get("/{identifier}", description="Retrieve a given record of Coverage given a unique identifier")
def retrieve_coverage(model: schemas.Coverage = Depends(adapters.retrieve_coverage)) -> \
        schemas.Coverage:
    return model


@coverage_router.post("/", description="Create a new record for Coverage using provided data")
def create_coverage(model: schemas.Coverage = Depends(adapters.create_coverage)) -> \
        schemas.Coverage:
    return model


@coverage_router.put("/update/{identifier}",
                     description="Update an existing record of Coverage given a unique identifier")
def update_coverage(model: schemas.Coverage = Depends(adapters.update_coverage)) -> \
        schemas.Coverage:
    return model


@coverage_router.delete("/delete/{identifier}",
                        description="Delete a record of Coverage given a unique identifier")
def destroy_coverage(deleted: Any = Depends(adapters.destroy_coverage)) -> Any:
    return deleted

from typing import List, Union, Any, Dict

from fastapi import Path, Body, Form

from django.db import models as django_models

from variant import models
from variant.models import schemas

from util.models.schemas import DeleteData


## Variant
def list_variants() -> List[schemas.Variant]:
    queryset = iter(models.Variant.objects.all())
    fields = [x for x in schemas.Variant.__fields__.keys()]
    result = []
    for y in queryset:
        values = {x: getattr(y, x) for x in fields}
        record = schemas.Variant(**values)
        result.append(record)
    return result


def retrieve_variant(
        identifier: str = Path(..., description="Unique identifier for retrieving a Variant data")) -> \
        models.Variant:
    instance = models.Variant.objects.filter(slug=identifier).first()
    if instance:
        fields = [x for x in schemas.Variant.__fields__.keys()]
        values = {x: getattr(instance, x) for x in fields}
        return schemas.Variant(**values)


def retrieve_filter_variant(
        identifier: Dict = Body(..., description="Unique identifier for retrieving a Variant data")
) -> List[models.Variant]:
    sample = identifier.get('SampleSelect')
    library = identifier.get('LibrarySelect')
    severity = identifier.get('SeveritySelect')
    tiers = identifier.get('TierSelect')
    variant_pass = identifier.get('OutcomeSelect')
    slug = identifier.get('slug')

    queryset = models.Variant.objects.all()

    if slug:
        queryset = queryset.filter(slug=slug)
    if sample:
        queryset = queryset.filter(sample=sample)
    if library:
        queryset = queryset.filter(library=library)
    if severity:
        queryset = queryset.filter(severity=severity)
    if tiers:
        if tiers == '1&2':
            queryset = queryset.filter(tiers__in=['1', '2', '1&2', 'tier1', 'tier2', 'tier1 and 2'])
        else:
            queryset = queryset.filter(tiers=tiers)
    if variant_pass:
        queryset = queryset.filter(variant_pass=variant_pass)

    if queryset:
        return schemas.Variant.from_queryset(queryset)


def create_variant(instance: schemas.Variant = Body(
    ...,
    description="Instance of the model schema or field-value mapping of the target model to be created")) -> \
        Union[schemas.Variant, None]:
    fields = [str(x.name) for x in models.Variant._meta.get_fields()]
    values = {x: getattr(instance, x) for x in fields}
    record = models.Variant.objects.create(**values)
    if record:
        return instance


def destroy_variant(
        identifier: str = Path(..., description="Unique identifier for retrieving a User instance"),
        data: DeleteData = Body(
            None,
            description="Unique identifier for retrieving a User instance or extra data needed to "
                        "delete selected instance")) -> Any:
    if data:
        # perform validation for deleting
        queryset = models.Variant.objects.filter(slug=identifier)
        instance = queryset.first()
        queryset.filter(sample=instance.sample, reference_genome=instance.reference_genome, chr=instance.chr).delete()
        if instance:
            fields = [str(x.name) for x in models.Variant._meta.get_fields()]
            values = {x: getattr(instance, x) for x in fields}
            return schemas.Variant(**values)


def update_variant(
        identifier: str = Path(..., description="Unique identifier for retrieving a User instance"),
        instance: Dict = Body(
            ...,
            description="Instance of the model schema or field-value mapping of the target model to be updated")) -> \
        Union[schemas.Variant, None]:
    queryset = models.Variant.objects.filter(sample=identifier)

    if queryset and instance:
        # remove primary keys of variant
        if instance.get('reference_genome'):
            instance.pop('reference_genome')
        if instance.get('pos'):
            instance.pop('pos')
        if instance.get('ref'):
            instance.pop('ref')
        if instance.get('alt'):
            instance.pop('alt')
        if instance.get('sample'):
            instance.pop('sample')
        if instance.get('library_name'):
            instance.pop('library_name')
        if instance.get('run_id'):
            instance.pop('run_id')
        if instance.get('chr'):
            instance.pop('chr')
        model = queryset.first()
        if model:
            model.update(**instance)
            return schemas.Variant.from_orm(model)


def multiple_update_variant(records: Dict = Body(..., description="List of records to update")) -> Any:
    # {identifier: 0, interpretation: dfsdfsdf}
    variants = records.get('records')
    updates = []
    for variant in variants:
        model = models.Variant.objects.filter(slug=variant.get('identifier')).first()
        if model:
            model.interpretation = variant.get('interpretation')
            model.save()
            updates.append(schemas.Variant.from_orm(model))

    return updates


# Simple Variant
def list_sample_variant() -> List[schemas.VariantSample]:
    queryset = iter(models.VariantSample.objects.all())
    fields = [x for x in schemas.VariantSample.__fields__.keys()]
    result = []
    for y in queryset:
        values = {x: getattr(y, x) for x in fields}
        record = schemas.VariantSample(**values)
        result.append(record)
    return result


def retrieve_sample_variant(
        identifier: str = Path(..., description="Unique identifier for retrieving a Variant data")) -> \
        models.VariantSample:
    instance = models.VariantSample.objects.filter(
        django_models.Q(slug=identifier) &
        django_models.Q(sample=identifier) &
        django_models.Q(run_id=identifier) &
        django_models.Q(libraries=identifier)).first()
    if instance:
        fields = [x for x in schemas.VariantSample.__fields__.keys()]
        values = {x: getattr(instance, x) for x in fields}
        return schemas.VariantSample(**values)


def search_sample_variant(
        search: Dict = Body(..., description="Search parameter for retrieving a Sample Variant data")) -> \
        List[schemas.VariantSample]:
    queryset = models.VariantSample.objects.all()

    identifier = search.get('identifier')
    queryset = queryset.filter(sample=identifier)
    if not queryset.exists():
        queryset = queryset.filter(libraries=identifier)
    if not queryset.exists():
        queryset = queryset.filter(run_id=identifier)
    if queryset:
        fields = [x for x in schemas.VariantSample.__fields__.keys()]
        result = []
        for y in queryset:
            values = {x: getattr(y, x) for x in fields}
            record = schemas.VariantSample(**values)
            result.append(record)
        return result


def create_sample_variant(instance: schemas.VariantSample = Body(
    ...,
    description="Instance of the model schema or field-value mapping of the target model to be created")) -> \
        Union[schemas.VariantSample, None]:
    fields = [str(x.name) for x in models.VariantSample._meta.get_fields()]
    values = {x: getattr(instance, x) for x in fields}
    record = models.VariantSample.objects.create(**values)
    if record:
        return instance


def destroy_sample_variant(
        identifier: str = Path(..., description="Unique identifier for retrieving a User instance"),
        data: DeleteData = Body(
            None,
            description="Unique identifier for retrieving a User instance or extra data needed to "
                        "delete selected instance")) -> Any:
    if data:
        # perform validation for deleting
        queryset = models.VariantSample.objects.filter(slug=identifier)
        instance = queryset.first()
        queryset.filter(reference_genome=instance.reference_genome, run_id=instance.run_id,
                        sample=instance.sample).delete()
        if instance:
            fields = [str(x.name) for x in models.VariantSample._meta.get_fields()]
            values = {x: getattr(instance, x) for x in fields}
            return schemas.VariantSample(**values)


def update_sample_variant(
        identifier: str = Path(..., description="Unique identifier for retrieving a User instance"),
        instance: schemas.VariantSample = Body(
            ...,
            description="Instance of the model schema or field-value mapping of the target model to be updated")) -> \
        Union[schemas.VariantSample, None]:
    queryset = models.VariantSample.objects.filter(slug=identifier)
    data = instance.dict()

    if queryset and data:
        # remove primary keys of sample variant
        data.pop('sample')
        data.pop('run_id')
        data.pop('reference_genome')
        data.pop('library_name')
        data.pop('chr')
        data.pop('pos')
        data.pop('ref')
        data.pop('alt')
        data.pop('date_annotated')
        model = queryset.first()
        if model:
            model.update(**data)
        return instance


# Coverage
def list_coverage() -> List[schemas.Coverage]:
    queryset = iter(models.Coverage.objects.all())
    fields = [x for x in schemas.Coverage.__fields__.keys()]
    result = []
    for y in queryset:
        values = {x: getattr(y, x) for x in fields}
        record = schemas.Coverage(**values)
        result.append(record)
    return result


def retrieve_coverage(
        identifier: str = Path(..., description="Unique identifier for retrieving a Variant data")) -> \
        models.Coverage:
    instance = models.Coverage.objects.filter(slug=identifier).first()
    if instance:
        fields = [x for x in schemas.Coverage.__fields__.keys()]
        values = {x: getattr(instance, x) for x in fields}
        return schemas.Coverage(**values)


def create_coverage(instance: schemas.Coverage = Body(
    ...,
    description="Instance of the model schema or field-value mapping of the target model to be created")) -> \
        Union[schemas.Coverage, None]:
    fields = [str(x.name) for x in models.Coverage._meta.get_fields()]
    values = {x: getattr(instance, x) for x in fields}
    record = models.Coverage.objects.create(**values)
    if record:
        return instance


def destroy_coverage(
        identifier: str = Path(..., description="Unique identifier for retrieving a User instance"),
        data: DeleteData = Body(
            None,
            description="Unique identifier for retrieving a User instance or extra data needed to "
                        "delete selected instance")) -> Any:
    if data:
        # perform validation for deleting
        queryset = models.Coverage.objects.filter(slug=identifier)
        instance = queryset.first()
        queryset.filter(reference_genome=instance.reference_genome, run_id=instance.run_id,
                        sample=instance.sample).delete()
        if instance:
            fields = [str(x.name) for x in models.Coverage._meta.get_fields()]
            values = {x: getattr(instance, x) for x in fields}
            return schemas.Coverage(**values)


def update_coverage(
        identifier: str = Path(..., description="Unique identifier for retrieving a User instance"),
        instance: schemas.Coverage = Body(
            ...,
            description="Instance of the model schema or field-value mapping of the target model to be updated")) -> \
        Union[schemas.Coverage, None]:
    queryset = models.Coverage.objects.filter(slug=identifier)
    data = instance.dict()

    if queryset and data:
        # remove primary keys of sample variant
        data.pop('slug')
        data.pop('amplicon')
        data.pop('library_name')
        model = queryset.first()
        if model:
            model.update(**data)
        return instance

from typing import Mapping, TypeVar

from cassandra.cqlengine import columns
from django_cassandra_engine.models import DjangoCassandraModel

from pydantic import BaseModel as PydanticSchema


Schema = TypeVar('Schema', bound=PydanticSchema)


class Variant(DjangoCassandraModel):
    slug = columns.Text(primary_key=True)
    sample = columns.Text(index=True, primary_key=True)
    library = columns.Text(index=True, primary_key=True)
    gene = columns.Text(index=True)
    amplicon = columns.Text()
    ref = columns.Text(primary_key=True)
    alt = columns.Text(primary_key=True)
    codon_change = columns.Text()
    aa_change = columns.Text()
    max_som_aaf = columns.Text()
    num_times_db = columns.Integer()
    num_times_run = columns.Integer()
    median_vaf_db = columns.Float()
    median_vaf_run = columns.Float()
    std_dev_vaf = columns.Float()
    vaf_percentile_ranker = columns.Float()
    callers = columns.Text()
    callers_count = columns.Text()
    num_cosmic_samples = columns.Text()
    cosmic_ids = columns.Text()
    cosmic_aa = columns.Text()
    clinvar_significance = columns.Text()
    clinvar_hgvs = columns.Text()
    clinvar_disease = columns.Text()
    coverage = columns.Text()
    num_reads = columns.Integer()
    impact = columns.Text()
    severity = columns.Text()
    maximum_population_af = columns.Integer()
    min_caller_depth = columns.Integer()
    max_caller_depth = columns.Integer()
    chrom = columns.Integer()
    start = columns.Integer()
    end = columns.Integer()
    rs_ids = columns.Text()
    matching_samples_in_run = columns.Text()
    MuTect_AF = columns.Text()
    VarDict_AF = columns.Text()
    FreeBayes_AF = columns.Text()
    Scalpel_AF = columns.Text()
    Platypus_AF = columns.Text()
    Pindel_AF = columns.Text()
    tiers = columns.Text()
    variant_pass = columns.Text()
    interpretation = columns.Text()

    class Meta:
        get_pk_field = 'slug'


class VariantSample(DjangoCassandraModel):
    slug = columns.Text(primary_key=True)
    sample = columns.Text(primary_key=True)
    libraries = columns.Text(primary_key=True)
    run_id = columns.Text(primary_key=True)
    reporting_templates = columns.Text()
    minimum_reportable_somatic_allele_frequency = columns.Text()
    minimum_amplicon_depth = columns.Float()
    maximum_popularity_allele_frequency = columns.Float()
    
    @classmethod
    def from_dict(cls, data: Mapping):
        return cls.objects.create(**data)

    @classmethod
    def from_schema(cls, data: Schema):
        return cls.from_dict(data.dict())

    class Meta:
        get_pk_field = 'slug'


class Coverage(DjangoCassandraModel):
    slug = columns.Text(primary_key=True)
    amplicon = columns.Text(primary_key=True)
    library_name = columns.Text(primary_key=True)
    sample = columns.Text()
    num_reads = columns.Integer()
    mean_coverage = columns.Float()
    database_median = columns.Float()
    database_std_dev = columns.Float()

    @classmethod
    def from_dict(cls, data: Mapping):
        return cls.objects.create(**data)

    @classmethod
    def from_schema(cls, data: Schema):
        return cls.from_dict(data.dict())

    class Meta:
        get_pk_field = 'slug'

from typing import List, Optional
from pydantic import BaseModel
from django_cassandra_engine.models import DjangoCassandraModel


class Variant(BaseModel):
    slug: str
    sample: str
    library: str
    gene: str
    amplicon: Optional[str]
    ref: str
    alt: str
    codon_change: Optional[str] = None
    aa_change: Optional[str] = None
    max_som_aaf: Optional[str] = None
    num_times_db: Optional[int] = None
    num_times_run: Optional[int] = None
    median_vaf_db: Optional[float] = None
    median_vaf_run: Optional[float] = None
    std_dev_vaf: Optional[float] = None
    vaf_percentile_ranker: Optional[float] = None
    callers: Optional[str] = None
    callers_count: Optional[str] = None
    num_cosmic_samples: Optional[str] = None
    cosmic_ids: Optional[str] = None
    cosmic_aa: Optional[str] = None
    clinvar_significance: Optional[str] = None
    clinvar_hgvs: Optional[str] = None
    clinvar_disease: Optional[str] = None
    coverage: Optional[str] = None
    num_reads: Optional[int] = None
    impact: Optional[str] = None
    severity: Optional[str] = None
    maximum_population_af: Optional[int] = None
    min_caller_depth: Optional[int] = None
    max_caller_depth: Optional[int] = None
    chrom: Optional[int] = None
    start: Optional[int] = None
    end: Optional[int] = None
    rs_ids: Optional[str] = None
    matching_samples_in_run: Optional[str] = None
    MuTect_AF: Optional[str] = None
    VarDict_AF: Optional[str] = None
    FreeBayes_AF: Optional[str] = None
    Scalpel_AF: Optional[str] = None
    Platypus_AF: Optional[str] = None
    Pindel_AF: Optional[str] = None
    tiers: Optional[str] = None
    variant_pass: Optional[str] = None
    interpretation: Optional[str] = None

    @classmethod
    def from_queryset(cls, queryset: List[DjangoCassandraModel]):
        return [cls.from_orm(x) for x in queryset if isinstance(x, DjangoCassandraModel)]

    class Config:
        orm_mode = True


class VariantSample(BaseModel):
    slug: str
    sample: str
    libraries: str
    run_id: str
    reporting_templates: Optional[str]
    minimum_reportable_somatic_allele_frequency: Optional[str]
    minimum_amplicon_depth: Optional[float]
    maximum_popularity_allele_frequency: Optional[float]

    @classmethod
    def from_queryset(cls, queryset: List[DjangoCassandraModel]):
        return [cls.from_orm(x) for x in queryset if isinstance(x, DjangoCassandraModel)]

    class Config:
        orm_mode = True


class Coverage(BaseModel):
    slug: str
    sample: str
    library_name: str
    amplicon: str
    num_reads: Optional[int]
    mean_coverage: Optional[float]
    database_median: Optional[float]
    database_std_dev: Optional[float]

    @classmethod
    def from_queryset(cls, queryset: List[DjangoCassandraModel]):
        return [cls.from_orm(x) for x in queryset if isinstance(x, DjangoCassandraModel)]

    class Config:
        orm_mode = True

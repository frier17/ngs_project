from django.apps import AppConfig


class BamLinksConfig(AppConfig):
    name = 'bam_links'

from typing import List, Union, Any, Dict

from fastapi import Path, Body

from django.db import models as django_models

from cnvs import models
from cnvs.models import schemas

from util.models.schemas import DeleteData


# CNV
def list_cnvs() -> List[schemas.Cnv]:
    queryset = iter(models.Cnv.objects.all())
    fields = [x for x in schemas.Cnv.__fields__.keys()]
    result = []
    for y in queryset:
        values = {x: getattr(y, x) for x in fields}
        record = schemas.Cnv(**values)
        result.append(record)
    return result


def retrieve_cnv(
        identifier: str = Path(..., description="Unique identifier for retrieving a Cnv data")) -> \
        models.Cnv:
    instance = models.Cnv.objects.filter(
        django_models.Q(slug=identifier) |
        django_models.Q(gene=identifier) |
        django_models.Q(cnv_filter=identifier)).first()
    if instance:
        fields = [x for x in schemas.Cnv.__fields__.keys()]
        values = {x: getattr(instance, x) for x in fields}
        return schemas.Cnv(**values)


def create_cnv(instance: schemas.Cnv = Body(
    ...,
    description="Instance of the model schema or field-value mapping of the target model to be created")) -> \
        Union[schemas.Cnv, None]:
    fields = [str(x.name) for x in models.Cnv._meta.get_fields()]
    values = {x: getattr(instance, x) for x in fields}
    record = models.Cnv.objects.create(**values)
    if record:
        return instance


def destroy_cnv(
        identifier: str = Path(..., description="Unique identifier for retrieving a User instance"),
        data: DeleteData = Body(
            None,
            description="Unique identifier for retrieving a User instance or extra data needed to "
                        "delete selected instance")) -> Any:
    if data:
        # perform validation for deleting
        queryset = models.Cnv.objects.filter(slug=identifier)
        instance = queryset.first()
        respone = queryset.filter(gene=instance.gene, chrom=instance.chrom, cnv_filter=instance.cnv_filter).delete()
        if respone:
            fields = [str(x.name) for x in models.Cnv._meta.get_fields()]
            values = {x: getattr(instance, x) for x in fields}
            return schemas.Cnv(**values)


def update_cnv(
        identifier: str = Path(..., description="Unique identifier for retrieving a User instance"),
        instance: Dict = Body(
            ...,
            description="Instance of the model schema or field-value mapping of the target model to be updated")) -> \
        Union[schemas.Cnv, None]:
    queryset = models.Cnv.objects.filter(slug=identifier)

    if queryset and instance:
        # remove primary keys of variant
        instance.pop('slug')
        model = queryset.first()
        if model:
            model.update(**instance)
        return schemas.Cnv(**instance)


# Fusion
def list_fusion() -> List[schemas.Fusion]:
    queryset = iter(models.Fusion.objects.all())
    fields = [x for x in schemas.Fusion.__fields__.keys()]
    result = []
    for y in queryset:
        values = {x: getattr(y, x) for x in fields}
        record = schemas.Fusion(**values)
        result.append(record)
    return result


def retrieve_fusion(
        identifier: str = Path(..., description="Unique identifier for retrieving a Fusion data")) -> \
        models.Fusion:
    instance = models.Fusion.objects.filter(
        django_models.Q(slug=identifier) |
        django_models.Q(annots=identifier) |
        django_models.Q(splice_type=identifier) |
        django_models.Q(fusion_name=identifier)).first()
    if instance:
        fields = [x for x in schemas.Fusion.__fields__.keys()]
        values = {x: getattr(instance, x) for x in fields}
        return schemas.Fusion(**values)


def create_fusion(instance: schemas.Fusion = Body(
    ...,
    description="Instance of the model schema or field-value mapping of the target model to be created")) -> \
        Union[schemas.Fusion, None]:
    fields = [str(x.name) for x in models.Fusion._meta.get_fields()]
    values = {x: getattr(instance, x) for x in fields}
    record = models.Fusion.objects.create(**values)
    if record:
        return instance


def destroy_fusion(
        identifier: str = Path(..., description="Unique identifier for retrieving a User instance"),
        data: DeleteData = Body(
            None,
            description="Unique identifier for retrieving a User instance or extra data needed to "
                        "delete selected instance")) -> Any:
    if data:
        # perform validation for deleting
        queryset = models.Fusion.objects.filter(slug=identifier)
        instance = queryset.first()
        respone = queryset.filter(fusion_name=instance.fusion_name, splice_type=instance.splice_type,
                                  annots=instance.annots).delete()
        if respone:
            fields = [str(x.name) for x in models.Fusion._meta.get_fields()]
            values = {x: getattr(instance, x) for x in fields}
            return schemas.Fusion(**values)


def update_fusion(
        identifier: str = Path(..., description="Unique identifier for retrieving a User instance"),
        instance: Dict = Body(
            ...,
            description="Instance of the model schema or field-value mapping of the target model to be updated")) -> \
        Union[schemas.Fusion, None]:
    queryset = models.Fusion.objects.filter(slug=identifier)

    if queryset and instance:
        # remove primary keys of variant
        instance.pop('slug')
        model = queryset.first()
        if model:
            model.update(**instance)
        return schemas.Fusion(**instance)
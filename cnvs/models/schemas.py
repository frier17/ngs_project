from typing import Optional
from pydantic import BaseModel


class Cnv(BaseModel):
    slug: str
    chrom: Optional[str]
    cnv_pos: Optional[str]
    pos_end: Optional[int]
    gene: Optional[str]
    qual: Optional[int]
    cnv_filter: Optional[str]
    full_change: Optional[float]


class Fusion(BaseModel):
    slug: Optional[str]
    fusion_name: Optional[str]
    junction_read_count: Optional[int]
    spanning_frag_count: Optional[int]
    est_j: Optional[float]
    est_s: Optional[float]
    splice_type: Optional[str]
    left_gene: Optional[str]
    left_break_point: Optional[str]
    right_gene: Optional[str]
    right_break_point: Optional[str]
    junction_reads: Optional[str]
    spanning_frags: Optional[str]
    large_anchor_support: Optional[str]
    ffpm: Optional[float]
    left_break_dinuc: Optional[str]
    left_break_entropy: Optional[float]
    right_break_dinuc: Optional[str]
    right_break_entropy: Optional[float]
    annots: Optional[str]





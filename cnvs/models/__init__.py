from typing import Mapping, TypeVar

from cassandra.cqlengine import columns
from django_cassandra_engine.models import DjangoCassandraModel

from pydantic import BaseModel as PydanticSchema

Schema = TypeVar('Schema', bound=PydanticSchema)


class Cnv(DjangoCassandraModel):
    slug = columns.Text(primary_key=True)
    chrom = columns.Text()
    cnv_pos = columns.Integer()
    pos_end = columns.Integer()
    gene = columns.Text()
    qual = columns.Integer()
    cnv_filter = columns.Text()
    full_change = columns.Float()

    @classmethod
    def from_dict(cls, data: Mapping):
        return cls.objects.create(**data)

    @classmethod
    def from_schema(cls, data: Schema):
        return cls.from_dict(data.dict())

    class Meta:
        get_pk_field = 'slug'


class Fusion(DjangoCassandraModel):
    slug = columns.Text(primary_key=True)
    fusion_name = columns.Text()
    junction_read_count = columns.Integer()
    spanning_frag_count = columns.Integer()
    est_j = columns.Float()
    est_s = columns.Float()
    splice_type = columns.Text()
    left_gene = columns.Text()
    left_break_point = columns.Text()
    right_gene = columns.Text()
    right_break_point = columns.Text()
    junction_reads = columns.Text()
    spanning_frags = columns.Text()
    large_anchor_support = columns.Text()
    ffpm = columns.Float()
    left_break_dinuc = columns.Text()
    left_break_entropy = columns.Float()
    right_break_dinuc = columns.Text()
    right_break_entropy = columns.Float()
    annots = columns.Text()

    @classmethod
    def from_dict(cls, data: Mapping):
        return cls.objects.create(**data)

    @classmethod
    def from_schema(cls, data: Schema):
        return cls.from_dict(data.dict())

    class Meta:
        get_pk_field = 'slug'

from typing import List, Any

from fastapi import APIRouter, Depends

from cnvs import models
from cnvs.models import schemas
from cnvs.models import adapters

cnv_router = APIRouter()
fusion_router = APIRouter()


## CNV
@cnv_router.get("/", description="List all entries of Cnv")
def list_cnv(
        model: List[schemas.Cnv] = Depends(adapters.list_cnvs)) -> List[schemas.Cnv]:
    return model


@cnv_router.get("/{identifier}", description="Retrieve a given record of Cnv given a unique identifier")
def retrieve_cnv(model: schemas.Cnv = Depends(adapters.retrieve_cnv)) -> schemas.Cnv:
    return model


@cnv_router.post("/", description="Create a new record for Cnv using provided data")
def create_cnv(model: schemas.Cnv = Depends(adapters.create_cnv)) -> schemas.Cnv:
    return model


@cnv_router.put("/update/{identifier}",
                            description="Update an existing record of Cnv given a unique identifier")
def update_cnv(model: models.Cnv = Depends(adapters.update_cnv)) -> schemas.Cnv:
    return model


@cnv_router.delete("/delete/{identifier}", description="Delete a record of Cnv given a unique identifier")
def destroy_cnv(deleted: Any = Depends(adapters.destroy_cnv)) -> Any:
    return deleted


## Fusion
@fusion_router.get("/", description="List all entries of Fusion")
def list_fusion(
        model: List[schemas.Fusion] = Depends(adapters.list_fusion)) -> List[schemas.Fusion]:
    return model


@fusion_router.get("/{identifier}", description="Retrieve a given record of Fusion given a unique identifier")
def retrieve_fusion(model: models.Fusion = Depends(adapters.retrieve_fusion)) -> schemas.Fusion:
    return model


@fusion_router.post("/", description="Create a new record for Fusion using provided data")
def create_fusion(model: models.Fusion = Depends(adapters.create_fusion)) -> schemas.Fusion:
    return model


@fusion_router.put("/update/{identifier}",
                            description="Update an existing record of Fusion given a unique identifier")
def update_fusion(model: models.Fusion = Depends(adapters.update_fusion)) -> schemas.Fusion:
    return model


@fusion_router.delete("/delete/{identifier}", description="Delete a record of Fusion given a unique identifier")
def destroy_fusion(deleted: Any = Depends(adapters.destroy_fusion)) -> Any:
    return deleted

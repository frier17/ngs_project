from django.apps import AppConfig


class FusionsConfig(AppConfig):
    name = 'fusions'

"""
ASGI config for ngs_project project.

It exposes the ASGI callable as a module-level variable named ``application``.

For more information on this file, see
https://docs.djangoproject.com/en/3.0/howto/deployment/asgi/
"""

import os

from django.core.asgi import get_asgi_application
from fastapi import FastAPI, Request
from fastapi.staticfiles import StaticFiles
from fastapi.responses import HTMLResponse
from fastapi.middleware.cors import CORSMiddleware
from fastapi.templating import Jinja2Templates

os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'ngs_project.settings')

application = get_asgi_application()

from variant.routers import variant_router, sample_variant_router, coverage_router
from cnvs.routers import cnv_router, fusion_router

fastapp = FastAPI()

fastapp.add_middleware(
    CORSMiddleware,
    allow_origins=["*"],
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
    expose_headers=["*"]
)

fastapp.mount("/static", StaticFiles(directory="static"), name="static")

templates = Jinja2Templates(directory="templates")


@fastapp.get("/bam.html", response_class=HTMLResponse)
async def bam(request: Request):
    return templates.TemplateResponse("bam.html", {"request": request})


@fastapp.get('/cnv.html', response_class=HTMLResponse)
async def cnv(request: Request):
    return templates.TemplateResponse('cnv.html', {'request': request})


@fastapp.get('/fusions.html', response_class=HTMLResponse)
async def bam(request: Request):
    return templates.TemplateResponse('fusions.html', {'request': request})


@fastapp.get('/index.html', response_class=HTMLResponse)
async def index(request: Request):
    return templates.TemplateResponse('index.html', {'request': request})


@fastapp.get('/', response_class=HTMLResponse)
async def index(request: Request):
    return templates.TemplateResponse('index.html', {'request': request})


@fastapp.get('/qc.html', response_class=HTMLResponse)
async def qc(request: Request):
    return templates.TemplateResponse('qc.html', {'request': request})


@fastapp.get('/report.html', response_class=HTMLResponse)
async def report(request: Request):
    return templates.TemplateResponse('report.html', {'request': request})


@fastapp.get('/sample.html', response_class=HTMLResponse)
async def sample(request: Request):
    return templates.TemplateResponse('sample.html', {'request': request})


@fastapp.get('/variants.html', response_class=HTMLResponse)
async def variants(request: Request):
    return templates.TemplateResponse('variants.html', {'request': request})


## Variant package
fastapp.include_router(variant_router, tags=["Variant DNA data"], prefix="/variant")
fastapp.include_router(sample_variant_router, tags=["Sample Variant DNA data"], prefix="/sample")
fastapp.include_router(coverage_router, tags=["Target Variant DNA data"], prefix="/coverage")

## Cnvs
fastapp.include_router(cnv_router, tags=["CNV data"], prefix="/cnv")
fastapp.include_router(fusion_router, tags=["Fusion DNA data"], prefix="/fusion")

const BASE_URL = '';

// Use the axios API for all application calls to the server. This is more robust and designed for API calls

// Get Sample Info
function httpGetSampleInfo(theUrl, callback) {
	var xmlHttp = new XMLHttpRequest();

	xmlHttp.onreadystatechange = function() {
	    if (xmlHttp.readyState == 4 && xmlHttp.status == 200) {
	        callback(xmlHttp.responseText);
	        var sampleInfo = JSON.parse(xmlHttp.responseText);
	        sample = '';

	        for (var i = 0; i < sampleInfo.length; i++) {
	        	sample += '<option value="'+sampleInfo[i].sample+'">';
		        libraries = sampleInfo[i].libraries;
		        run_id = sampleInfo[i].run_id;
		        reporting_templates = sampleInfo[i].reporting_templates;
		        mrsaf = sampleInfo[i].minimum_reportable_somatic_allele_frequency;
		        mad = sampleInfo[i].minimum_amplicon_depth;
		        mpaf = sampleInfo[i].maximum_popularity_allele_frequency;
		        sampleName = sampleInfo[i].sample;
	        }

	        document.getElementById('SampleOptions').innerHTML = sample;
	        document.getElementById('api_sample_library').innerHTML = libraries;
	        document.getElementById('api_sample_run').innerHTML = run_id;
	        document.getElementById('api_sample_template').innerHTML = reporting_templates;
	        document.getElementById('api_sample_mrsaf').innerHTML = mrsaf;
	        document.getElementById('api_sample_mad').innerHTML = mad;
	        document.getElementById('api_sample_mpaf').innerHTML = mpaf;
	        document.getElementById('apiSampleName').innerHTML = sampleName;
			}
	}

	xmlHttp.open("GET", theUrl, true);
    xmlHttp.send(null);
}

// httpGetSampleInfo('js/sample.json', function(response) {
httpGetSampleInfo(BASE_URL + '/sample', function(response) {
})

var slug = '';

function prepareVariant(variants) {
    //var variants = JSON.parse(xmlHttp.responseText);
    /*
    var output = '<textarea id="slugsCount" hidden>'+variants.length+'</textarea><textarea id="variants"
    hidden>'+variants+'</textarea>';*/
    var output = '';
    output += '<form method="POST" class="form-group" onsubmit="UpdateInterp(event)" id="variantForm" name="variantForm" >'

    for (var i = 0; i < variants.length; i++) {
        output += '<tr>';
        output += '<th scope="row">'
        +variants[i].library+
        '<textarea id="SID' + variants[i].slug + '" hidden name="updates[]">'+variants[i].slug+'</textarea></th>';
        output += '<td>'
        + variants[i].interpretation +
        '<select class="form-control form-control-sm textsize d-inline" onchange="pushSelection(event)"'
        + 'name="' + variants[i].slug + '" >'
        + '<option value=""></option>'
        + '<option value="Mutation">Mutation</option>'
        + '<option value="SNP">SNP</option>'
        + '<option value="Artifact">Artifact</option></select></td>';
        output += '<td>'+variants[i].gene+'</td>';
        output += '<td>'+variants[i].amplicon+'</td>';
        output += '<td>'+variants[i].ref+'</td>';
        output += '<td>'+variants[i].alt+'</td>';
        output += '<td>'+variants[i].codon_change+'</td>';
        output += '<td>'+variants[i].aa_change+'</td>';
        output += '<td>'+variants[i].max_som_aaf+'</td>';
        output += '<td>'+variants[i].num_times_db+'</td>';
        output += '<td>'+variants[i].num_times_run+'</td>';
        output += '<td>'+variants[i].median_vaf_db+'</td>';
        output += '<td>'+variants[i].median_vaf_run+'</td>';
        output += '<td>'+variants[i].callers+'</td>';
        output += '<td>'+variants[i].cosmic_ids+'</td>';
        output += '<td>'+variants[i].num_cosmic_samples+'</td>';
        output += '<td>'+variants[i].clinvar_significance+'</td>';
        output += '<td>'+variants[i].min_caller_depth+'</td>';
        output += '<td>'+variants[i].max_caller_depth+'</td>';
        output += '</tr>';
        // slug = variants[i].slug;
    }
    output += '</form>'
    return output;
}
// Get Variants Data
function httpGetVariants(theUrl, callback) {
	//var xmlHttp = new XMLHttpRequest();
	axios.get(BASE_URL + '/variant/').then(response => {
	    let variants = response.data;
	    const output = prepareVariant(variants);
	    document.getElementById('api_variants_data').innerHTML = output;
	}).catch(error => {
	    console.log(error);
	});
    /*
	xmlHttp.onreadystatechange = function() {
	    if (xmlHttp.readyState == 4 && xmlHttp.status == 200) {
	        callback(xmlHttp.responseText);
	        output = prepareVariant(xmlHttp)
	        document.getElementById('api_variants_data').innerHTML = output;
			}
	}

	xmlHttp.open("GET", theUrl, true);
    xmlHttp.send(null);
    */
}

function filterVariants(event) {
	// Get the form values, post to endpoint and update variant table
	event.preventDefault();
	let sample = document.getElementById('SampleSelect').value;
	let library = document.getElementById('LibrarySelect').value;
	let tier = document.getElementById('TierSelect').value;
	let outcome = document.getElementById('OutcomeSelect').value;
	let severity = document.getElementById('SeveritySelect').value;
	let payload = {
        'SampleSelect': sample,
        'LibrarySelect': library,
        'TierSelect': tier,
        'OutcomeSelect': outcome,
        'SeveritySelect': severity
	};

	axios.post(BASE_URL + '/variant/filter', payload).then(response => {
	    let filtered = response.data;

	    if(filtered) {
	        output = prepareVariant(filtered);
	        document.getElementById('api_variants_data').innerHTML = output;
	    }
	}).catch(error => {
	    console.log(error)
	})
}

// httpGetVariants('js/variant.json', function(response) {
httpGetVariants(BASE_URL + '/variant/', function(response) {
})



// Get CNV Data
function httpGetCNV(theUrl, callback) {
	var xmlHttp = new XMLHttpRequest();

	xmlHttp.onreadystatechange = function() {
	    if (xmlHttp.readyState == 4 && xmlHttp.status == 200) {
	        callback(xmlHttp.responseText);
	        var cnvs = JSON.parse(xmlHttp.responseText);
	        var cnv_output = '';

	        for (var i = 0; i < cnvs.length; i++) {
		        cnv_output += '<tr>';
		        cnv_output += '<th scope="row">'+cnvs[i].chrom+'</th>';
		        cnv_output += '<td>'+cnvs[i].cnv_pos+'</td>';
		        cnv_output += '<td>'+cnvs[i].pos_end+'</td>';
		        cnv_output += '<td>'+cnvs[i].gene+'</td>';
		        cnv_output += '<td>'+cnvs[i].qual+'</td>';
		        cnv_output += '<td>'+cnvs[i].cnv_filter+'</td>';
		        cnv_output += '<td>'+cnvs[i].full_change+'</td>';
		        cnv_output += '</tr>';
	        }

	        document.getElementById('cnvtable').innerHTML = cnv_output;
			}
	}

	xmlHttp.open("GET", theUrl, true);
    xmlHttp.send(null);
}

// httpGetCNV('js/cnv.json', function(response) {
httpGetCNV(BASE_URL + '/cnv/', function(response) {
})


// Get Fusion Data
function httpGetFusion(theUrl, callback) {
	var xmlHttp = new XMLHttpRequest();

	xmlHttp.onreadystatechange = function() {
	    if (xmlHttp.readyState == 4 && xmlHttp.status == 200) {
	        callback(xmlHttp.responseText);
	        var fusions = JSON.parse(xmlHttp.responseText);
	        var fusion_output = '';

	        for (var i = 0; i < fusions.length; i++) {
		        fusion_output += '<tr>';

		        fusion_output += '<th scope="row">'+fusions[i].fusion_name+'</th>';
		        fusion_output += '<td>'+fusions[i].junction_read_count+'</td>';
		        fusion_output += '<td>'+fusions[i].spanning_frag_count+'</td>';
		        fusion_output += '<td>'+fusions[i].est_j+'</td>';
		        fusion_output += '<td>'+fusions[i].est_s+'</td>';
		        fusion_output += '<td>'+fusions[i].splice_type+'</td>';
		        fusion_output += '<td>'+fusions[i].left_gene+'</td>';
		        fusion_output += '<td>'+fusions[i].left_break_point+'</td>';
		        fusion_output += '<td>'+fusions[i].right_gene+'</td>';
		        fusion_output += '<td>'+fusions[i].right_break_point+'</td>';

		        fusion_output += '<td>'+fusions[i].junction_reads+'</td>';
		        fusion_output += '<td>'+fusions[i].spanning_frags+'</td>';
		        fusion_output += '<td>'+fusions[i].ffpm+'</td>';
		        fusion_output += '<td>'+fusions[i].left_break_dinuc+'</td>';
		        fusion_output += '<td>'+fusions[i].left_break_entropy+'</td>';
		        fusion_output += '<td>'+fusions[i].right_break_dinuc+'</td>';
		        fusion_output += '<td>'+fusions[i].right_break_entropy+'</td>';
		        fusion_output += '<td>'+fusions[i].annots+'</td>';

		        fusion_output += '</tr>';
	        }

	        document.getElementById('api_fusions_data').innerHTML = fusion_output;
			}
	}

	xmlHttp.open("GET", theUrl, true);
    xmlHttp.send(null);
}

// httpGetFusion('js/fusion.json', function(response) {
httpGetFusion(BASE_URL + '/fusion/', function(response) {
})


// Get Coverage Data
function httpGetCoverage(theUrl, callback) {
	var xmlHttp = new XMLHttpRequest();

	xmlHttp.onreadystatechange = function() {
	    if (xmlHttp.readyState == 4 && xmlHttp.status == 200) {
	        callback(xmlHttp.responseText);
	        var coverages = JSON.parse(xmlHttp.responseText);
	        var coverage_output = '';

	        for (var i = 0; i < coverages.length; i++) {
	        coverage_output += '<tr>';
	        coverage_output += '<th scope="row">'+coverages[i].sample+'</th>';
	        coverage_output += '<td>'+coverages[i].library_name+'</td>';
	        coverage_output += '<td>'+coverages[i].amplicon+'</td>';
	        coverage_output += '<td>'+coverages[i].num_reads+'</td>';
	        coverage_output += '<td>'+coverages[i].mean_coverage+'</td>';
	        coverage_output += '<td>'+coverages[i].database_median+'</td>';
	        coverage_output += '<td>'+coverages[i].database_std_dev+'</td>';
	        coverage_output += '</tr>';
	        }

	        document.getElementById('api_coverages_data').innerHTML = coverage_output;
			}
	}

	xmlHttp.open("GET", theUrl, true);
    xmlHttp.send(null);
}

// httpGetCoverage('js/coverage.json', function(response) {
httpGetCoverage(BASE_URL + '/coverage/', function(response) {
})


// Update Variant Data

var records = [];

function UpdateInterp() {
    // Get the form object. iterate through its elements for hidden input with name slugs[]
    // iterate through elements for select. Match select with input. Remove entries without select
	event.preventDefault();
	if (updates) {
	    axios.post(BASE_URL + '/variant/update/multiple', {'records': updates}).then(response => {
	        const output = prepareVariant(variants);
	        document.getElementById('api_variants_data').innerHTML = output;
	        alert('Interpretation saved successfully');
	        location.replace('variants.html');

	    }).catch(error => {
	        console.log(error);
	    });
	}

}
var updates = [];
function pushSelection(event) {
    updates.push({"identifier": event.target.name, "interpretation": event.target.value})
}

function prepareSampleSearch(samples) {
    let out = "<li class='list-group-item'><div class='card my-5'><div class='card-body'>";
    for (const sample of samples) {
        out += "<dl>" + "<dt>Sample</dt><dd>" + sample.sample + "</dd>";
        out += "<dt>Libraries</dt><dd>" + sample.libraries + "</dd>";
        out += "<dt>Run ID</dt><dd>" + sample.run_id + "</dd>";
        out += "</dl>";
    }
    out += "</div></div></li>";
    return out;
}

function searchSample(event) {
    event.preventDefault();
    let query = document.getElementById("sample").value;
    if (query) {
        axios.post(BASE_URL + '/sample/search/', {'identifier': query}).then(response => {
            const out = prepareSampleSearch(response.data);
            document.getElementById("search-result-list").innerHTML = out;
        }).catch(error => {
            console.log(error);
        });
    }

}

function resetSearch(event) {
    document.getElementById("search-result-list").innerHTML = null;
}

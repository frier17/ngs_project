import json
from pydoc import locate
from typing import Dict, Optional, Union

from django.db import models
from pydantic import BaseModel, Field


class BaseHeading(BaseModel):
    """
    @deprecated: see updated variant store
    Schema for DNA data sequence heading. This will have fields that includes:
    1. Sample
    2. Libraries
    3. Run ID
    4. Reporting Templates
    5. Minimum Reportable Somatic Allele Frequency
    6. Minimum Amplicon Depth
    7. Maximum Population Allele Frequency
    """
    sample: str = Field(..., description="Description of this term can come here")
    libraries: str = Field(..., description="")
    runId: str = Field(..., description="Tip or description of the field")
    reportingTemplate: str = Field(..., description="")
    mrsaf: str = Field(..., description="Minimum Reportable Somatic Allele Frequency")
    mad: str = Field(..., description="Minimum Amplicon Depth")
    mpaf: str = Field(..., description="Maximum Population Allele Frequency")


class DeleteData(BaseModel):
    data: Dict
    message: str = Field(None, max_length=100)

    class Config:
        arbitrary_types_allowed = True


class HttpServiceResponse:
    data: Dict
    message: str
    error: Optional[Union[str, bool]]
    exception: None

    def __init__(self, arg):
        if isinstance(arg, models.Model):
            # Get the application schema for given model
            name = f'{arg.__module__}.schemas.{arg.__class__.__name__}'
            schema = locate(name)
            if schema and hasattr(schema, 'from_orm'):
                self.data = schema.from_orm(arg)
            else:
                self.data = vars(arg)
        elif isinstance(arg, Exception):
            self.success = False
            self.exception = arg.__traceback__
            self.message = f'Invalid data structure. Expected mapping or model instance but got {type(arg)}'
        else:
            self.data = arg
        super(HttpServiceResponse, self).__init__()

    def response(self):
        # return data as schema or dict
        return json.dumps(self.data, skipkeys=True)

    def __str__(self):
        if self.success:
            return "[Success]"
        return f'[Exception] "{self.exception}"'

    def __repr__(self):
        if self.success:
            return "<HttpServiceResult Success>"
        return f"<HttpServiceResult Exception {self.exception}>"

    def __enter__(self):
        return json.dumps(self.data, skipkeys=True)

    def __exit__(self, *kwargs):
        pass


def handle_result(result: HttpServiceResponse):
    if not result.success:
        with result as exception:
            raise exception
    with result as result:
        return result
